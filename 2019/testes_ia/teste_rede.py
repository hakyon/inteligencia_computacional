
import numpy as np  # linear algebra
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
import matplotlib.pyplot as plt
import seaborn as sns


class NeuralNetwork(object):
    def __init__(self):

        df = pd.read_csv('master.csv', sep=',', encoding='utf-8', quotechar='"',
                         decimal=',')

        # Normalizacao

        # df.describe()

        # normalizacao de alguns campos
        self.df = self.mapping(df, "sex")
        self.df = self.mapping(df, "country")
        self.df = self.mapping(df, "age")
        self.df = self.mapping(df, "country-year")
        self.df = self.mapping(df, "generation")
        self.df['gdp_for_year'] = [col.replace(',', '') for col in self.df['gdp_for_year']]


        df = df.fillna(0)

        df.head()

        y = (df['suicides/100k pop']).astype("float") / 10
        y = y.astype("int")
        x = df.drop(['suicides/100k pop'], axis=1)

        # removendo campos inuteis

        df = df.drop(['gdp_per_capita', 'population', 'suicides_no', 'year', 'country-year',
                      'suicides/100k pop', 'generation', 'HDI for year', 'gdp_for_year'], axis=1)

        x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25, random_state=27)

        clf = MLPClassifier(activation='logistic', # tipo de funçao de ativacao ,
                            hidden_layer_sizes=(100, 100, 100,100,100,100),# definição do numero de camadas de neuronios, 3 camadas com 100
                            max_iter=500,  # numero maximo de iteracoes para atingir melhor resultado
                            alpha=0.0001,  # parametro de penalidade
                            solver='adam',  # metodo de otimizacao de peso
                            verbose=True,  # parametro para impressao da progressao
                            random_state=12,  # a semente usada para geracao randomica
                            tol=0.000000001,# tolerancia para otimizacao , quando sua taxa de melhora eh inferior, eh usado como regra de parada
                            learning_rate='adaptive'  # taxa de apredizado , adaptativo eh 0,001
                            )

        clf.fit(x_train, y_train)
        y_pred = clf.predict(x_test)

        print('Accuracy : {:.2f} %'.format(accuracy_score(y_test, y_pred) * 100))
        cm = confusion_matrix(y_test, y_pred)

        sns.heatmap(cm, center=True)
        sns.
        plt.show()

    def mapping(self, data, feature):
        """

        :param data: method for normalize each data in Dataframe
        :param feature:
        :return:
        """
        featureMap = dict()
        count = 0
        for i in sorted(data[feature].unique(), reverse=True):
            featureMap[i] = count
            count = count + 1
        data[feature] = data[feature].map(featureMap)
        return data


def main():
    algoritm = NeuralNetwork()


if __name__ == "__main__":
    main()
