import matplotlib.pyplot as plt
import numpy as np
import random


class NeuralNetwork(object):
    def __init__(self):
        # Pesos (Sinapses)
        self.w = [0, 0] # pesos
        # limiar
        self.limiar = 0
        # Bias
        self.bias = 1
        self.taxa_aprendizado = 1
        self.numero_maximo_iteracoes = 10

    def start(self):
        x = [
            [self.bias, 1, 1],
            [self.bias, 1, 0],
            [self.bias, 0, 1],
            [self.bias, 0, 1],
        ]

        # Saida esperada
        #y = 0
        y = [
            [1, 0],
            [0, 0],
            [0, 1],
            [1, 1],
        ]
        # Color - Red or Blue, 1 and -1, respectively
        color = ""

        # Data Dictionary
        data_dictionary = {
            # 'Keys' : 'Values',
            '0.72,0.82': '-1',
            '0.91,-0.69': '-1',
            '0.03,0.93': '-1',
            '0.12,0.25': '-1',
            '0.96,0.47': '-1',
            '0.8,-0.75': '-1',
            '0.46,0.98': '-1',
            '0.66,0.24': '-1',
            '0.72,-0.15': '-1',
            '0.35,0.01': '-1',
            '-0.11,0.1': '1',
            '0.31,-0.96': '1',
            '0.0,-0.26': '1',
            '-0.43,-0.65': '1',
            '0.57,-0.97': '1',
            '-0.72,-0.64': '1',
            '-0.25,-0.43': '1',
            '-0.12,-0.9': '1',
            '-0.58,0.62': '1',
            '-0.77,-0.76': '1'
        }

        # Liga o modo grafico
        plt.ion()

        for k in range(1, self.numero_maximo_iteracoes):
            hits = 0
            print("\n------------------------- INTERACAO " + str(k) + " ------------------------- ")

            for i in range(0, len(x)):
                sum = 0

                # Soma dos pesos
                for j in range(0, len(x[i]) - 1):
                    sum += x[i][j] * self.w[j]

                # Atualiza a saida  = Bias + Peso somado
                output = self.bias + sum

                # Saida determinada pelo limiar
                if output > self.limiar:
                    y = 1
                else:
                    y = -1

                    # atualiza os pesos se a saida nao chegar ao esperado
                if y == x[i][2]:
                    hits += 1
                    answer = "Correcao!"
                else:
                    for j in range(0, len(self.w)):
                        self.w[j] = self.w[j] + (self.taxa_aprendizado * x[i][2] * x[i][j])
                    self.bias = self.bias + self.taxa_aprendizado * x[i][2]
                    answer = "Erro - Atualizando o peso do noh: " + str(self.w)

                # Imprimindo interacao
                if y == 1:
                    print("\n" + answer)
                elif y == -1:
                    print("\n" + answer)

                # Plot
                plt.clf()  # Limpando
                plt.title('Iteracao %s\n' % (str(k)))
                plt.grid(False)  # imprimindo a grida na tela
                plt.xlim(-1, 1)  # Set x-axis
                plt.ylim(-1, 1)  # Set y-axis

                xA = 1
                xB = -1

                if self.w[1] != 0:
                    yA = (- self.w[0] * xA - self.bias) / self.w[1]
                    yB = (- self.w[0] * xB - self.bias) / self.w[1]
                else:
                    try:
                        xA = - self.bias / self.w[0]
                    except ZeroDivisionError:
                        xA = 0

                    try:
                        xB = - self.bias / self.w[0]
                    except ZeroDivisionError:
                        xB = 0


                    #xA = - self.bias / self.w[0]
                    #xB = - self.bias / self.w[0]

                    yA = 1
                    yB = -1

                # Plot the black line, that is, we want to learn the black line as faithfully as possible
                # ("Best" Decision Boundary)
                plt.plot([0.77, -0.55], [-1, 1], color='k', linestyle='-', linewidth=1)

                # Generates our green line, that is, our learning line (Decision Boundary)
                plt.plot([xA, xB], [yA, yB], color='g', linestyle='-', linewidth=2)

                # Plot blue points
                x_coords, y_coords = self.get_points_of_color(data_dictionary, '-1')
                plt.plot(x_coords, y_coords, 'bo')

                # Plot red points
                x_coords, y_coords = self.get_points_of_color(data_dictionary, '1')
                plt.plot(x_coords, y_coords, 'ro')

                # Highlights the current point
                if answer == 'Correto!':
                    # Correct - with green color
                    plt.plot(x[i][0], x[i][1], 'go', markersize=15, alpha=.5)
                else:
                    # Incorrect - with magenta color
                    plt.plot(x[i][0], x[i][1], 'mo', markersize=30, alpha=.5)

                plt.show()

                # We were able to control the loop time, so a figure will be updated and displayed
                plt.pause(0.15)

            if hits == len(x):
                print("\n---------------------------------------------------------------")
                print("\nFuncionalidade aprendida com " + str(k) + " iteracoes!")
                break;
            '''
            else:
                print("\n---------------------------------------------------------------")
                print("\nFuncionalidade nao aprendida!")
                break;
            '''

        print("\nConcluido!\n")

    def get_points_of_color(self, data, label):
        x_coords = [float(point.split(",")[0]) for point in data.keys() if data[point] == label]
        y_coords = [float(point.split(",")[1]) for point in data.keys() if data[point] == label]
        return x_coords, y_coords


def main():
    rede = NeuralNetwork()
    rede.start()


if __name__ == "__main__":
    main()
