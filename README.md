### Universidade Tuiuti do Parana ###
### Ciência da Computação ###
### Disciplina Inteligencia Computacional ###

### Exercicios/Trabalhos ###

* Periodo : 02/2018 - 07/2018
* Version 1.00
* Ambiente : Desktop

### Ferramentas usadas ###

* Terminal : Python 3.5 , PyCharm
* Plataforma : Linux/Windows

### Assuntos ###

* KNN
* Kmeans
* Redes Neurais
* Redes Neurais em multicamadas
* Algoritmo Genético
* Nuvem de Particulas



### Autor  ###

* Alan Azevedo Bancks

### Contato ###

* E-mail: dsalan@hotmail.com
